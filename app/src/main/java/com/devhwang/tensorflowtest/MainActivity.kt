package com.devhwang.tensorflowtest

import android.app.Activity
import android.content.res.AssetFileDescriptor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

class MainActivity : AppCompatActivity() {

    var tfLite: Interpreter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tfLite = Interpreter(loadModelFile(this@MainActivity))

        mBtnInfer.setOnClickListener {
            var prediction:Float = doInference(input.text.toString())
            output.text = prediction.toString()
        }

    }

    fun doInference(inputString : String):Float{
        var inputVal = FloatArray(1)
        inputVal[0] = java.lang.Float.valueOf(inputString)

        var outputVal = Array(1) { FloatArray(1) }
        tfLite?.run(inputVal,outputVal)
        var inferredValue:Float = outputVal[0][0]
        return inferredValue
    }

    /** Memory-map the model file in Assets. */
    private fun loadModelFile(activity: Activity): MappedByteBuffer {
        //Open the model using an input stream, and memory map it to load
        var fileDescriptor: AssetFileDescriptor = activity.assets.openFd("mobilenet_quant_v1_224.tflite")
        var inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        var fileChannel:FileChannel = inputStream.channel
        var startOffset  = fileDescriptor.startOffset
        var declaredLength = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY,startOffset,declaredLength)
    }
}
